# mxc4005xc.py (MXC4005XC I2C Accelerometer)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython library supports the MXC4005XC I2C Accelerometer.

Implementation Notes
--------------------
**Hardware:**
    * the MXC4005XC I2C Accelerometer
        * Mouser:  https://www.mouser.com/ProductDetail/MEMSIC/MXC4005XC?qs=RcG8xmE7yp3iyYgWLmAtUw%3D%3D
        * DigiKey: https://www.digikey.com/en/products/detail/memsic-inc/MXC4005XC/10322569

**Usage:**
    from mxc4005xc import MXC4005XC
    gyro = MXC4005XC(scl=board.SCL, sda=board.SCA, interrupt=GP20)

"""

import time
import busio
import digitalio

class MXC4005XC:
    '''
        The MXC4005XC is a tiny affordable 3-axis accelerometer with temperature sensor
        the accelerometer has 12bit values for each axis.

        The class currently assumes the accelerometer is set for +/- 2G
    '''

    DEFAULT_ADDR        = 0x15
    INTERRUPT_SHAKE     = 0x00
    INTERRUPT_TILT      = 0x01
    READ_STATUS         = 0x02
    STATUS_READY        = 0b00010000
    READ_X              = 0x03
    READ_Y              = 0x05
    READ_Z              = 0x07
    READ_T              = 0x09
    SET_SHAKE_INT       = 0x0A
    SET_TILT_INT        = 0x0B
    READ_SETTINGS       = 0x0D
    WRITE_SETTINGS      = 0x0D
    # settings are a bit field
    SETTING_POWERDOWN   = 0b00000001
    SETTING_POWERUP     = 0b00000000
    SETTING_RANGE       = 0b01100000
    SETTING_2G          = 0b00000000
    SETTING_4G          = 0b00100000
    SETTING_8G          = 0b01000000
    SETTING_SHAKE       = 0b00001111 # shake -Y +Y -X +X
    SETTING_ORIENT      = 0b11000000 # orientation Z and XY
    SETTING_INTERRUPT   = (SETTING_ORIENT | SETTING_SHAKE)

    def __init__(self, scl=None, sda=None, interrupt=None, addr=DEFAULT_ADDR, i2c=None):
        print("mxc4005xc constructor:", scl, sda, interrupt, addr, i2c)
        if i2c is None and scl is None and sda is None:
            print("Error: must provide either i2c or scl+sda parameters")
            while True:
                pass    # infinite loop; don't allow code to continue
        if i2c is None:
            self.__i2c = busio.I2C(scl, sda)
        else:
            self.__i2c = i2c
        '''
        while not self.__i2c.try_lock():
            pass
        '''
        self.__addr = addr
        self.__range = 0
        self.__power = 0
        self.__data = bytearray(2)
        self.__x = 0    # x axis (raw)
        self.__y = 0    # y axis (raw)
        self.__z = 0    # z axis (raw)
        self.__t = 0    # temperature (celsius)
        time.sleep(0.2) # this may not be 100% necessary but we give the device some time to settle
        self.__i2c.unlock()
        val = self.__read_byte(MXC4005XC.READ_SETTINGS)
        self.__power = val & MXC4005XC.SETTING_POWERDOWN
        self.__range = (0x01 << (((val & MXC4005XC.SETTING_RANGE) >> 5) + 1)) # 2, 4, or 8
        self.__interrupt = interrupt
        if interrupt:
            self.__interrupt = digitalio.DigitalInOut(interrupt)
            self.__interrupt.direction = digitalio.Direction.INPUT
            val = self.__write_byte(MXC4005XC.SET_SHAKE_INT, MXC4005XC.SETTING_INTERRUPT)
        # there are several things which can be detected; we simplify the API by generalizing to just two actions
        self.__orientation = 0
        self.__shake = False

    def __repr__(self):
        return "MXC4005XC: address:0x{:02X}: range:{:d} power:{} x:{:+05d} y:{:+05d} z:{:+05d} temperature:{:02d}".format(self.__addr, self.__range, ("DN" if self.__power else "UP"), self.__x, self.__y, self.__z, self.__t)

    def __write_byte(self, cmd, val):
        ''' internal method to write a command and a byte '''
        try:
            # print ("I2C Write: %02X %02X" % (cmd, val))
            if self.__i2c.try_lock():
                # command and data must be sent together for proper timing
                self.__i2c.writeto(self.__addr, bytes([cmd, val]))
                self.__i2c.unlock()
            else:
                self.__i2c.unlock()
                print ("********  I2C Write lock error  ********")
        except: # OSError as e:
            print ("********  I2C Write error  ********")
        return val


    def __read_byte(self, cmd):
        ''' internal method to write a command and read a byte '''
        val = 0
        try:
            # print ("I2C Read: %02X" % (cmd))
            if self.__i2c.try_lock():
                self.__i2c.writeto(self.__addr, bytes([cmd]))
                self.__i2c.readfrom_into(self.__addr, self.__data)
                val = self.__data[0]
                self.__i2c.unlock()
            else:
                self.__i2c.unlock()
                print ("********  I2C Read lock error  ********")
        except: # OSError as e:
            print ("********  I2C Read error  ********")
        return val

    def __read_doublebyte(self, cmd):
        ''' internal method to write two successive commands and read a byte each; assumes only high 12 bits are significant '''
        val = self.__read_byte(cmd)                 # read MSB
        val = (val << 8) + self.__read_byte(cmd+1)  # add MSB and LSB
        val = val // 16                             # shift out the empty 4 bits, preserving the sign
        if val > 2047:
            val = val - 4096                        # adjust to 2's compliment
        return val

    @property
    def I2C(self):
        # return the I2C decide so it may be shared
        return self.__i2c

    def update(self):
        ''' recompute all data '''
        self.__x = self.__read_doublebyte(MXC4005XC.READ_X)
        self.__y = self.__read_doublebyte(MXC4005XC.READ_Y)
        self.__z = self.__read_doublebyte(MXC4005XC.READ_Z)

        self.__t = 0
        self.__t = self.__read_byte(MXC4005XC.READ_T)
        if self.__t > 127:
            self.__t = self.__t - 256
        self.__t += 25  # the base temperature reading is 25C

        RANGE = 1024    # the datasheet implies this should be 2048
        self.__p = self.__x / (RANGE * (-90.0))
        self.__r = self.__y / (RANGE * (-90.0))
        if (self.__z < 0):
            if (self.__y < 0):
                self.__r = 180.0 - self.__r
            else:
                self.__r = -180.0 - self.__r
        # print ("Data: X:%+5d    Y:%+5d    Z:%+5d    P:%+5.1f    R:%+5.1f    T:%2d" % (self.__x, self.__y, self.__z, self.__p, self.__r, self.__t))


    @property
    def interrupt(self):
        ''' True if the interrupt has been triggered '''
        if self.__interrupt.value == 0: # indicates the interrupt has occured
            val = self.__read_byte(MXC4005XC.INTERRUPT_SHAKE)                           # read the data
            self.__write_byte(MXC4005XC.INTERRUPT_SHAKE, MXC4005XC.SETTING_INTERRUPT)   # clear the data
            self.__shake = val & MXC4005XC.SETTING_SHAKE
            self.__orientation = (val & MXC4005XC.SETTING_ORIENT) >> 6 # we want to top 2 bits
            return True
        return False

    @property
    def shake(self):
        ''' really only useful if used after 'interrupt' '''
        return self.__shake

    @property
    def orientation(self):
        ''' really only useful if used after 'interrupt' '''
        return self.__orientation

    @property
    def event(self):
        ''' read the source of the interrupt; will also clear the interrupt '''
        pass

    @property
    def x(self):
        ''' read current X axis value and return it '''
        # the datasheet says this is +/- 2048 but I'm only seeing +/- 1024
        return self.__x

    @property
    def y(self):
        ''' read current Y axis value and return it '''
        # the datasheet says this is +/- 2048 but I'm only seeing +/- 1024
        return self.__y

    @property
    def z(self):
        ''' read current Z axis value and return it '''
        return self.__z

    @property
    def temperature(self):
        ''' read current temperature value and return it '''
        return self.__t

    @property
    def range(self):
        ''' G range and power state '''
        val = self.__read_byte(MXC4005XC.READ_SETTINGS)
        power = val & MXC4005XC.SETTING_POWERDOWN
        # return 2, 4, or 8
        return (0x01 << (((val & MXC4005XC.SETTING_RANGE) >> 5) + 1))
    @range.setter
    def range(self, range):
        if range <= 2:
            range = 0
        elif range <= 4:
            range = 1
        else:
            range = 2
        range = range << 5
        self.__write_byte(MXC4005XC.WRITE_SETTINGS, range)

    @property
    def power(self):
        ''' G range and power state '''
        val = self.__read_byte(MXC4005XC.READ_SETTINGS)
        return (val & MXC4005XC.SETTING_POWERDOWN)
    @power.setter
    def power(self, value):
        if value:
            ''' power up the chip after it hs been powered down '''
            self.__write_byte(MXC4005XC.WRITE_SETTINGS, MXC4005XC.SETTING_POWERUP)
        else:
            ''' power down the chip '''
            self.__write_byte(MXC4005XC.WRITE_SETTINGS, MXC4005XC.SETTING_POWERDOWN)


    @property
    def pitch(self):
        return self.__p

    @property
    def roll(self):
        return self.__r
