# simpletimer.py (SimpleTimer)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython library provides a simple timer.
It uses supervisor.ticks_ms() and corrects for wrap around

Code derived from:
https://docs.circuitpython.org/en/latest/shared-bindings/supervisor/

Implementation Notes
--------------------

**Usage:**
    from simpletimer import SimpleTimer
    timer = SimpleTimer()
    timer.start(2000)
    while True:
        if timer.expired:
            # do something
            pass
        ...

"""


import supervisor

class SimpleTimer:
    _TICKS_PERIOD = const(1<<29)
    _TICKS_MAX = const(_TICKS_PERIOD-1)
    _TICKS_HALFPERIOD = const(_TICKS_PERIOD//2)

    def __init__(self, interval=100):
        self._interval = interval
        self._milliseconds = 0
        self._running = False

    @property
    def interval(self):
        return self._interval
    @interval.setter
    def interval(self, interval):
        self._interval = interval

    def start(self, interval=None):
        if interval is None:
            interval = self._interval
        self._milliseconds = supervisor.ticks_ms() + self._interval
        self._running = True
    def stop(self):
        self._milliseconds = 0
        self._running = False
    @property
    def expired(self):
        if self._running == False:
            return False
        # "Compute the signed difference between two ticks values, assuming that they are within 2**28 ticks"
        now = supervisor.ticks_ms()
        diff = (self._milliseconds - now) & _TICKS_MAX
        diff = ((diff + _TICKS_HALFPERIOD) & _TICKS_MAX) - _TICKS_HALFPERIOD
        if diff < 0:
            self.stop()
            return True
        return False
        
